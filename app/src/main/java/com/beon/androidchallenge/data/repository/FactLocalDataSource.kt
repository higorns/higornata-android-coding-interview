package com.beon.androidchallenge.data.repository

import com.beon.androidchallenge.domain.model.Fact

class FactLocalDataSource {

    fun insertFact(fact: Fact) {
        localData.add(fact)
    }

    fun getFact(): List<Fact> = localData.toList()

    companion object {
        private val localData = mutableListOf<Fact>()
    }

}

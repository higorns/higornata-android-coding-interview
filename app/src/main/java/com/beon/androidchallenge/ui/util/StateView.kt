package com.beon.androidchallenge.ui.util

sealed class StateView<T>

internal data class SuccessState<T>(val data: T) : StateView<T>()
internal  class ErrorState<T> : StateView<T>()
internal class LoadingState<T> : StateView<T>()
internal class EmptyState<T> : StateView<T>()
package com.beon.androidchallenge.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.beon.androidchallenge.data.repository.FactRepository
import com.beon.androidchallenge.domain.model.Fact
import com.beon.androidchallenge.ui.util.EmptyState
import com.beon.androidchallenge.ui.util.ErrorState
import com.beon.androidchallenge.ui.util.LoadingState
import com.beon.androidchallenge.ui.util.StateView
import com.beon.androidchallenge.ui.util.SuccessState
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    private val _currentFact =  MutableLiveData<StateView<Fact>>()
    val currentFact : LiveData<StateView<Fact>> = _currentFact
    private var lastFact : Fact? = null

    fun searchNumberFact(number: String) {

        _currentFact.postValue(LoadingState())

        if (number.isEmpty()) {
            _currentFact.postValue(EmptyState())
            return
        }

        viewModelScope.launch {
           // delay(300)

            FactRepository.getInstance().getFactForNumber(number, object : FactRepository.FactRepositoryCallback<Fact> {
                override fun onResponse(response: Fact) {
                    _currentFact.postValue(SuccessState(response))
                    lastFact = response
                }

                override fun onError() {
                    _currentFact.postValue(ErrorState())
                }

            })
        }
    }

    fun likeFact() {
        lastFact?.let { fact ->
            FactRepository.getInstance().insertFact(fact)
        }
    }
}
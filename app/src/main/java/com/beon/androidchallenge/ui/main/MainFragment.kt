package com.beon.androidchallenge.ui.main

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.beon.androidchallenge.R
import com.beon.androidchallenge.databinding.MainFragmentBinding
import com.beon.androidchallenge.ui.util.EmptyState
import com.beon.androidchallenge.ui.util.ErrorState
import com.beon.androidchallenge.ui.util.LoadingState
import com.beon.androidchallenge.ui.util.SuccessState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class MainFragment() : Fragment(), CoroutineScope {

    override val coroutineContext: CoroutineContext = Dispatchers.Main

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel
    private lateinit var binding: MainFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        initViews()
        initObservers()
    }

    private fun initViews() {
        binding.run {

            val watcher = object : TextWatcher {
                private var searchFor = ""

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    val searchText = s.toString().trim()
                    if (searchText == searchFor)
                        return

                    searchFor = searchText

                    launch {
                        delay(600)  //debounce timeOut
                        if (searchText != searchFor)
                            return@launch

                        viewModel.searchNumberFact(searchText)
                    }
                }

                override fun afterTextChanged(s: Editable?) = Unit
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
            }

            binding.numberEditText.addTextChangedListener(watcher)


            numberEditText.setOnEditorActionListener { textView, actionId, keyEvent ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    viewModel.searchNumberFact(textView.text.toString())
                    return@setOnEditorActionListener true
                } else {
                    return@setOnEditorActionListener false
                }
            }

            retryButton.setOnClickListener {
                viewModel.searchNumberFact(numberEditText.text.toString())
            }

            numberEditText.requestFocus()

            factTextView.setOnClickListener {
                 viewModel.likeFact()
            }
        }
    }

    private fun initObservers() {
        viewModel.currentFact.observeForever { fact ->

            when(fact) {
                is SuccessState -> {
                    hideProgress()
                    binding.factTextView.text = fact.data.text
                }

                is EmptyState -> {
                    hideProgress()
                    binding.factTextView.setText(R.string.instructions)
                }

                is LoadingState -> {
                    showProgress()
                }

                is ErrorState -> {
                    hideProgress()
                    binding.factTextView.setText(R.string.error)
                }
            }
        }
    }

    private fun showProgress() {
        binding.progressBar.isVisible = true
        binding.factTextView.isVisible = false
    }

    private fun hideProgress() {
        binding.progressBar.isVisible = false
        binding.factTextView.isVisible = true
    }

}
